#! /usr/bin/env bash


ERROR=$(tput setaf 1; echo -n "  [!]"; tput sgr0)
GOODTOGO=$(tput setaf 2; echo -n "  [✓]"; tput sgr0)
INFO=$(tput setaf 3; echo -n "  [-]"; tput sgr0)
RE=$(tput setaf 9; echo -n "  [***]"; tput sgr0)

print_usage() {
  echo "Usage: ./prepare.sh"
  exit 0
}


check_vagrant_path() {
  # Check for existence of Vagrant in PATH
  if ! which vagrant >/dev/null; then
    (echo >&2 "${ERROR} Vagrant was not found in your PATH.")
    (echo >&2 "${ERROR} Please correct this before continuing. Exiting.")
    (echo >&2 "${ERROR} Correct this by installing Vagrant with Homebrew or downloading from https://www.vagrantup.com/downloads.html")
    exit 1
  else
    (echo >&2 "${GOODTOGO} Vagrant was found in your PATH")
  fi

  
  # Ensure Vagrant >= 2.2.9
  # https://unix.stackexchange.com/a/285928
  VAGRANT_VERSION="$(vagrant --version | cut -d ' ' -f 2)"
  REQUIRED_VERSION="2.2.9"
  # If the version of Vagrant is not greater or equal to the required version
  if ! [ "$(printf '%s\n' "$REQUIRED_VERSION" "$VAGRANT_VERSION" | sort -V | head -n1)" = "$REQUIRED_VERSION" ]; then
    (echo >&2 "${ERROR} WARNING: It is highly recommended to use Vagrant $REQUIRED_VERSION or above before continuing")
  else 
    (echo >&2 "${GOODTOGO} Your version of Vagrant ($VAGRANT_VERSION) is supported")
  fi
}

# Returns 0 if not installed or 1 if installed
check_virtualbox_installed() {
  if which VBoxManage >/dev/null; then
    VBOX_VERSION="$(VBoxManage -v | cut -d ' ' -f 2)"
    (echo >&2 "${GOODTOGO} Your version of VirtualBox ($VBOX_VERSION) is supported")
  else
    (echo >&2 "${ERROR} VIRTUALBOX was not found in your PATH.")
    exit 1
  fi
}

check_macos_bigsur() {
  if sw_vers | grep ProductVersion | grep -c 11\. > /dev/null; then
    echo "1"
  else
    echo "0"
  fi
}


check_vagrant_reload_plugin() {
  # Ensure the vagrant-reload plugin is installed
  VAGRANT_RELOAD_PLUGIN_INSTALLED=$(vagrant plugin list | grep -c 'vagrant-reload')
  if [ "$VAGRANT_RELOAD_PLUGIN_INSTALLED" != "1" ]; then
    (echo >&2 "${ERROR} The vagrant-reload plugin is required and was not found. This script will attempt to install it now.")
    if ! $(which vagrant) plugin install "vagrant-reload"; then
      (echo >&2 "Unable to install the vagrant-reload plugin. Please try to do so manually and re-run this script.")
      exit 1
    else 
      (echo >&2 "${GOODTOGO} The vagrant-reload plugin was successfully installed!")
    fi
  else
    (echo >&2 "${GOODTOGO} The vagrant-reload plugin is currently installed")
  fi
}

# Check available disk space. Recommend 80GB free, warn if less.
check_disk_free_space() {
  FREE_DISK_SPACE=$(df -m "$HOME" | tr -s ' ' | grep '/' | cut -d ' ' -f 4)
  if [ "$FREE_DISK_SPACE" -lt 20000 ]; then
    (echo >&2 -e "${INFO} Warning: You appear to have less than 20GB of HDD space free on your primary partition. If you are using a separate parition, you may ignore this warning.\n")
    (echo >&2 "")
  else
    (echo >&2 "${GOODTOGO} You have more than 20GB of free space on your primary partition")
  fi
}

main() {
  # Get location of prepare.sh
  # https://stackoverflow.com/questions/59895/getting-the-source-directory-of-a-bash-script-from-within
  (echo >&2 "")
  VAGRANT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
  (echo >&2 " " "[+] Checking for necessary tools in PATH...")
  check_vagrant_path
  check_virtualbox_installed
  (echo >&2 "")
  (echo >&2 " " "[+] Checking for disk free space...")
  check_disk_free_space
  # shellcheck disable=SC2016
  (echo >&2 "${RE}"' To get started building Kubernetes Cluster, run `vagrant up`.')
  (echo >&2 '')
  (echo >&2 '')
  (echo >&2 '')
  (echo >&2 '')

}

main 

exit 0