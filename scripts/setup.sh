#!/bin/bash
#

set -e

cd /vagrant
pip3 install -r requirements.txt

cd /tmp
[ ! -d "kubespray" ] && git clone https://github.com/rrouzbeh/kubespray.git
cp -r /vagrant/inventory/kubelab kubespray/inventory/
cd kubespray

ansible-playbook -i inventory/kubelab/hosts.ini  --become cluster.yml 

cd /vagrant/
ansible-playbook -i inventory/kubelab/hosts.ini cluster.yaml

[ ! -d /home/vagrant/.kube ] && mkdir /home/vagrant/.kube
chown -R vagrant:vagrant /home/vagrant/.kube
mv /vagrant/inventory/kubelab/artifacts/admin.conf /home/vagrant/.kube/config
chmod a+x /vagrant/inventory/kubelab/artifacts/kubectl \
    && mv /vagrant/inventory/kubelab/artifacts/kubectl /usr/local/bin/kubectl

