#!/bin/bash
#

set -e

cd /vagrant/app-deployment
export KUBECONFIG=/home/vagrant/.kube/config
ansible-playbook deploy.yaml
