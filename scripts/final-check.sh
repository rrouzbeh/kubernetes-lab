#!/bin/bash
#

set -e

export KUBECONFIG=/home/vagrant/.kube/config

GRAFANA_IP=""
while [ -z $GRAFANA_IP ]; do
  echo "Waiting for Grafana to come up..."
  GRAFANA_IP=$(kubectl get svc grafana --template="{{range .status.loadBalancer.ingress}}{{.ip}}{{end}}")
  [ -z "$GRAFANA_IP" ] && sleep 10
done

HOST_IP=$(ip route get 1 | grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b" | tail -1)


show_final_information() {
  echo " "
  echo " "
  echo "******************************************************************************************************"
  echo "**  KUBERNETES WAS INSTALLED SUCCESSFULLY                              **"
  echo "**  USE THE FOLLOWING SETTINGS TO INTERACT WITH THE GRAFANA AND CLUSTER**"
  echo "******************************************************************************************************"
  echo "**"
  echo "**"
  echo "**  For Interacting with Kubernetes you have to connect "
  echo "**  to Control node using 'vagrant ssh control-node' "
  echo "**"
  echo "**"
  echo "**  GRAFANA URL: http://${GRAFANA_IP}:3000"
  echo "**  GRAFANA USER: admin"
  echo "**  GRAFANA PASSWORD: admin"
  echo "**"
  echo "**"
  echo "**"
  echo "**"
  echo "**" 
  echo "**"
  echo "**"
}

show_final_information

exit 0