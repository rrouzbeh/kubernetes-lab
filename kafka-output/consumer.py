
from datetime import datetime, timezone

from confluent_kafka import Consumer, KafkaException
from prometheus_client import Counter

from config.config import settings
from logger.logger import logger
from producer import TimeProducer





class TimeConsumer:
    TOPICS = [settings.get('INPUT_TOPIC')]
    CONFIG = {
        'bootstrap.servers': settings.get('KAFKA_BOOTSTRAP_SERVER'),
        'group.id': settings.get('KAFKA_GROUP_ID'),
        'session.timeout.ms': settings.get('KAFKA_SESSION_TIMEOUT_MS'),
        'auto.offset.reset': settings.get('KAFKA_AUTO_OFFSET_RESET')
    }
    consumed_message = Counter('output_consumed_message', 'Number of message consumed by output app')
    consumed_faild = Counter('output_consumed_faild', 'Number of message faild to consume by output app')
    
    def __init__(self):
        self.consumer = Consumer(self.CONFIG, logger=logger)
        self.producer = TimeProducer()
    
    def process_msg(self):
        try:
            time_in_millis = float(self.msg.value())
            dt = datetime.fromtimestamp(time_in_millis, tz=timezone.utc)
            rfc = dt.isoformat("T","milliseconds")
            self.producer.produce(rfc)
            self.consumed_message.inc()
        except Exception as exp:
            self.consumed_faild.inc()
            logger.error(exp)
            
        
    def consume(self):
        self.consumer.subscribe(self.TOPICS)
        try:
            while True:
                self.msg = self.consumer.poll(timeout=1.0)
                if self.msg is None:
                    continue
                if self.msg.error():
                    raise KafkaException(self.msg.error())
                else:
                    self.process_msg()
                   
        except KeyboardInterrupt:
            logger.error('Aborted by user')

