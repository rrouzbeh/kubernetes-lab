from confluent_kafka import Producer
from prometheus_client import Counter

from config.config import settings
from logger.logger import logger


class TimeProducer():
    CONF = {'bootstrap.servers': settings.get('KAFKA_BOOTSTRAP_SERVER')}
    TOPIC = settings.get('OUTPUT_TOPIC')
    produced_message = Counter('output_produced_message', 'Number of message produced by output app')
    delivery_faild = Counter('output_delivery_faild', 'Number of message failed to deliver by output app')
        
    def __init__(self):
        self.producer = Producer(**self.CONF)

    def delivery_report(self, err, msg):
        if err is not None:
            logger.error(msg)
            self.delivery_faild.inc()
        else:
            self.produced_message.inc()
    
    def produce(self, msg):
        try:
            self.producer.produce(topic=self.TOPIC, value=msg, callback=self.delivery_report)
            self.producer.poll(0)
        except BufferError as e:
            self.delivery_faild.inc()
            self.producer.poll(1)