
from prometheus_client import start_http_server

from consumer import TimeConsumer
from config.config import settings

def main():
    c = TimeConsumer()
    c.consume()
        

if __name__ == "__main__":
    start_http_server(settings.get('PROMETHEUS_PORT'))
    main()