# KUBERNETES LAB

## Instalation

### Requirements

* Vagrant 2.2.9+ (older versions may work but are not tested)
* Virtualbox 6.0+ (older versions may work but are not tested)
* **Processor/OS Architecture:**
  * 64-bit also known as x64, x86_64, AMD64 or Intel 64.
* **Cores:** Minimum of 8 cores (whether logical or physical)
* **Disk:** 20GB 
* **RAM:** Minimum 10GB
* **Applications:**
  * git

## Kubernetes lab Download
Run the following commands to clone the kubernetes-lab repo via git.

```bash
git clone https://gitlab.com/rrouzbeh/kubernetes-lab.git
```

## Kubernetes lab Install
In order to check the prerequisite the project comes with an script named **prepare.sh**. This script check automatically for all dependencies.

**To install Kubernetes lab:**  
Change your current directory location to the new kubernetes-lab directory, and first run the **prepare.sh** bash script as shown:

```bash
cd kubernetes-lab
```

If everything was ok, then run 'vagrant up'.

```bash
vagrant up
./install.sh
```

Remember that you can also access your kubernetes cluster by running the following commands:

```bash
vagrant ssh control-node
vagrant@control-node:~$ kubectl get node 
```


## Final Details
Once your kubernetes installation ends, you will be presented with information that you will need to access the grafana.

You will get the following information:

```
******************************************************************************************************
**  KUBERNETES WAS INSTALLED SUCCESSFULLY                              **
**  USE THE FOLLOWING SETTINGS TO INTERACT WITH THE GRAFANA AND CLUSTER**
******************************************************************************************************
**
**
**  For Interacting with Kubernetes you have to connect
**  to Control node using 'vagrant ssh control-node'
**
**
**  GRAFANA URL: http://172.18.8.200:3000
**  GRAFANA USER: admin
**  GRAFANA PASSWORD: admin
**
**
**
**
**
**
**
```



