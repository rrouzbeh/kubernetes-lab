import orjson as json

from confluent_kafka import Producer
from prometheus_client import Counter

from config.config import settings

class EpochProducer:
    CONF = {'bootstrap.servers': settings.get('KAFKA_BOOTSTRAP_SERVER')}
    TOPIC = settings.get('KAFKA_TOPIC')
    produced_message = Counter('produced_message', 'Number of message produced by EpochProducer')
    delivery_faild = Counter('delivery_faild', 'Number of message failed to deliver')
        
    def __init__(self):
        self.producer = Producer(**self.CONF)
    
    def delivery_report(self, err, msg):
        if err is not None:
            self.delivery_faild.inc()
        else:
            self.produced_message.inc()
    
    def produce(self, msg):
        try:
            self.producer.produce(topic=self.TOPIC, value=json.dumps(msg), callback=self.delivery_report)
            self.producer.poll(0)
        except BufferError as e:
            self.delivery_faild.inc()
            self.producer.poll(1)