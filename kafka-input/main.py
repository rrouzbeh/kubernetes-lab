import datetime
from time import sleep

import delorean
from prometheus_client import start_http_server

from config.config import settings
from producer import EpochProducer


def current_milli_time() -> int:
    return delorean.Delorean(datetime.datetime.utcnow(), timezone="UTC").epoch

def main():
    producer = EpochProducer()
    while True:
        sleep(settings.get('SLEEP'))
        producer.produce(current_milli_time())
        
        
if __name__ == "__main__":
    
    start_http_server(settings.get('PROMETHEUS_PORT'))
    main()